const sqlite3 = require('sqlite3').verbose();
const express = require('express');
const promisify = require('util').promisify;

const app = express();
const port = 1338;
let db;

try {
  db = new sqlite3.Database('./jenkins_stats.db');
} catch (e) {
  console.log("Unable to open './jenkins_stats.db'.");
  process.exit((Math.random() * 1000000 + 1) & 255);
}

// override that qt3.14
db.all = promisify(db.all);

function getStatusImage(status) {
  switch (status) {
    case 'started':
      return '<svg xmlns="http://www.w3.org/2000/svg" fill="#f0f0f0" width="24" height="24" viewBox="0 0 24 24"><path fill="none" d="M0 0h24v24H0z"/><path d="M13.49 5.48c1.1 0 2-.9 2-2s-.9-2-2-2-2 .9-2 2 .9 2 2 2zm-3.6 13.9l1-4.4 2.1 2v6h2v-7.5l-2.1-2 .6-3c1.3 1.5 3.3 2.5 5.5 2.5v-2c-1.9 0-3.5-1-4.3-2.4l-1-1.6c-.4-.6-1-1-1.7-1-.3 0-.5.1-.8.1l-5.2 2.2v4.7h2v-3.4l1.8-.7-1.6 8.1-4.9-1-.4 2 7 1.4z"/></svg>';
    case 'finished':
      return '<svg xmlns="http://www.w3.org/2000/svg" fill="#0c0" width="24" height="24" viewBox="0 0 24 24"><path fill="none" d="M0 0h24v24H0V0zm0 0h24v24H0V0z"/><path d="M16.59 7.58L10 14.17l-3.59-3.58L5 12l5 5 8-8zM12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm0 18c-4.42 0-8-3.58-8-8s3.58-8 8-8 8 3.58 8 8-3.58 8-8 8z"/></svg>';
    case 'failure':
      return '<svg xmlns="http://www.w3.org/2000/svg" fill="#d44" width="24" height="24" viewBox="0 0 24 24"><path d="M0 0h24v24H0z" fill="none"/><path d="M1 21h22L12 2 1 21zm12-3h-2v-2h2v2zm0-4h-2v-4h2v4z"/></svg>';
    case 'aborted':
      return '<svg xmlns="http://www.w3.org/2000/svg" fill="#eb5600" width="24" height="24" viewBox="0 0 24 24"><path fill="none" d="M0 0h24v24H0V0z"/><path d="M16 8v8H8V8h8m2-2H6v12h12V6z"/></svg>';
    default:
      return '<svg xmlns="http://www.w3.org/2000/svg" fill="#cc0" width="24" height="24" viewBox="0 0 24 24"><path fill="none" d="M0 0h24v24H0z"/><path d="M11 18h2v-2h-2v2zm1-16C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm0 18c-4.41 0-8-3.59-8-8s3.59-8 8-8 8 3.59 8 8-3.59 8-8 8zm0-14c-2.21 0-4 1.79-4 4h2c0-1.1.9-2 2-2s2 .9 2 2c0 2-3 1.75-3 5h2c0-2.25 3-2.5 3-5 0-2.21-1.79-4-4-4z"/></svg>';
  }
}

function timeConversion(millisec) {

  var seconds = (millisec / 1000).toFixed(1);
  var minutes = (millisec / (1000 * 60)).toFixed(1);
  var hours = (millisec / (1000 * 60 * 60)).toFixed(1);
  var days = (millisec / (1000 * 60 * 60 * 24)).toFixed(1);

  if (seconds < 60) {
    return `${Number(seconds).toFixed(0)} seconds`.replace(".", ",");
  } else if (minutes < 60) {
    return `${Number(minutes).toFixed(0)} minutes`.replace(".", ",");
  } else if (hours < 24) {
    return `${hours} hours`.replace(".", ",");
  } else {
    return `${days} days`.replace(".", ",");
  }
}

const lineChartQuery = "SELECT Cast (AVG(duration) As Integer) as 'avg_mins', COUNT(*) as 'builds', SUM(`duration`) as 'total_mins', `Day` \
    FROM( \
    SELECT \
    Cast ((JulianDay(MAX(timestamp)) - JulianDay(MIN(timestamp))) * 24 * 60 As Integer) as 'duration', \
    substr(timestamp,0,11) as 'day' \
    FROM jenkins_measurements GROUP BY job, number, day) \
    GROUP BY Day \
    LIMIT 50;";

const buildSummaryQuery = "SELECT SUM(`builds`) as 'sum_total', SUM(`total_mins`) as 'total_total_minutes', AVG(`avg_mins`) as 'avg_avg_minutes' FROM ( \
      SELECT Cast (AVG(duration) As Integer) as 'avg_mins', COUNT(*) as 'builds', SUM(`duration`) as 'total_mins', `Day` \
      FROM( \
      SELECT \
        Cast ((JulianDay(MAX(timestamp)) - JulianDay(MIN(timestamp))) * 24 * 60 As Integer) as 'duration', \
        substr(timestamp,0,11) as 'day' \
        FROM jenkins_measurements GROUP BY job, number, day) \
      GROUP BY Day \
    LIMIT 10);"

const tableSumQuery = "SELECT MIN(timestamp) as first_timestamp, MAX(timestamp) as last_timestamp, job, number, type, step, status, substr(timestamp,0,11) as 'day' FROM jenkins_measurements GROUP BY job, number, day ORDER BY first_timestamp DESC LIMIT 50;";

app.get('/', async (req, res, next) => {
  let avgMins = [];
  let builds = [];
  let totalMinutes = [];
  let days = [];

  const lastBuildsData = await db.all(tableSumQuery);
  const lineChartData = await db.all(lineChartQuery);
  const buildSum = await db.all(buildSummaryQuery);

  let buildSummaryTable = "<table><tr><th colspan=2>~30 day summary</th></tr>" +
    `<tr><td>Total builds</td><td>${buildSum[0].sum_total} builds</td></tr>` +
    `<tr><td>Total minutes</td><td>${new Intl.NumberFormat('de-DE').format(buildSum[0].total_total_minutes).replace(",", ".")} minutes</td></tr>` +
    `<tr><td>Average buildtime</td><td>${parseFloat(buildSum[0].avg_avg_minutes).toFixed(0)} minutes</td></tr></table>`;

  let lastBuildsTable = "<table id='sum'><tr><th>Start time</th><th>Runtime</th><th>Job</th><th></th><th>Latest step</th><th>Build-type</th></tr>";
  lastBuildsData.forEach(function (buildEntry) {
    let runtimeInMinutes = timeConversion(new Date(buildEntry.last_timestamp).getTime() - new Date(buildEntry.first_timestamp).getTime());
    lastBuildsTable += `<tr><td>${buildEntry.first_timestamp}</td/><td>${runtimeInMinutes}</td><td>${buildEntry.job} #${buildEntry.number}</td>
    <td>${getStatusImage(buildEntry.status)}</td><td>${buildEntry.step}</td><td>${buildEntry.type}</td></tr>\n`;
  });
  lastBuildsTable += "</table>";

  lineChartData.forEach(function (item) {
    avgMins.push(item['avg_mins']);
    builds.push(item['builds']);
    totalMinutes.push(item['total_mins']);
    days.push(`'${item['day']}'`);
  });

  const website = `<html>
      <head>
      <title>Charts</title>
      <style>
          html { 
              background-color: #4a4a4a;
              font-family: "Courier New", Courier, monospace;
              color: #f0f0f0;
              overflow: hidden;
          }
          table {
            border-collapse: collapse;
          }
          .part-left {
            float: left;  
          }
          .part-right {
            float: left;
            width: 25%;
            margin-left: 100px;
          }
          th, td {
            text-align: left;
            padding: 8px;
          }
          tr:nth-child(even) {background-color: #555;}
      </style>
      </head>
      <body>
      <div id="linechart">

      </div>
      <div id="piechart">

      </div>
      <script src="https://cdn.jsdelivr.net/npm/apexcharts@latest"></script>
      <script>
      var options = {
        theme: {
          mode: 'dark'
        },
        colors:['#cc0', '#eb5600', '#00c2cc'],
        chart: {
          height: 350,
          type: 'line',
          stacked: false,
          background: '#4a4a4a',
          animations: {
            enabled: false,
            easing: 'easeinout',
            speed: 800
          }
        },
        dataLabels: {
          enabled: false
        },
        series: [{
          name: 'Amount of builds',
          type: 'column',
          data: [${builds}]
        }, {
          name: 'Average buildtime',
          type: 'line',
          data: [${avgMins}]
        }, {
          name: 'Total buildtime',
          type: 'line',
          data: [${totalMinutes}]
        }],
        stroke: {
          width: [1, 4, 4]
        },
        xaxis: {
              color: '#cc0',
          categories: [${days}],
        },
        yaxis: [
          {
            axisTicks: {
              show: true,
            },
            axisBorder: {
              show: true,
              color: '#cc0'
            },
            labels: {
              style: {
                color: '#cc0',
              }
            },
            title: {
              text: "Builds"
            },
            tooltip: {
              enabled: true
            }
          },

          {
            seriesName: 'builds',
            opposite: true,
            axisTicks: {
              show: true,
            },
            axisBorder: {
              show: true,
              color: '#eb5600'
            },
            labels: {
              style: {
                color: '#eb5600',
              }
            },
            title: {
              text: "Average buildtime"
            },
          },
          {
            seriesName: 'builds II',
            opposite: true,
            axisTicks: {
              show: true,
            },
            axisBorder: {
              show: true,
              color: '#00c2cc'
            },
            labels: {
              style: {
                color: '#00c2cc',
              },
            },
            title: {
              text: "Total build minutes"
            }
          },
        ],
        tooltip: {
            enabled: true,
            followCursor: true
        },
        legend: {
          horizontalAlign: 'left',
          offsetX: 40
        }
      }

      var options2 = {
        chart: {
            type: 'donut',
        },
        series: [50, 50, 50],
        responsive: [{
            breakpoint: 480,
            options: {
                chart: {
                    width: 200
                },
                legend: {
                    position: 'bottom'
                }
            }
        }]
      }

      var linechart = new ApexCharts(
        document.querySelector("#linechart"),
        options
      );

      linechart.render();
      setTimeout(() => window.location.reload(true), 60000);
      </script>
      <b>Last 10 builds</b><br />
      <div>
      <div class="part-left">${lastBuildsTable}</div>
      <div class="part-right">${buildSummaryTable}</div>
      </div>
      </body>
      </html>`;

  return res.send(website);
});

console.log(`Hosting magic on: http://127.0.0.1:${port}/`)
app.listen(port);

