const sqlite3 = require('sqlite3').verbose();
const express = require('express');

const app = express();
const port = 1337;
let db;
try {
    db = new sqlite3.Database('./jenkins_stats.db');
} catch(e) {
    console.log("Unable to open './jenkins_stats.db'.");
    process.exit(1);
}

db.all('CREATE TABLE IF NOT EXISTS "jenkins_measurements" ( \
"node_name" TEXT, \
"job"	TEXT, \
"type"	TEXT, \
"number"	INTEGER, \
"status"	TEXT, \
"step"	TEXT, \
"timestamp"	DATETIME DEFAULT CURRENT_TIMESTAMP);');

app.get('/:node_name/:job/:type/:number/:step/:status', (req, res, next) => {
    try {
        console.log(`[${new Date().toISOString()}] ${req.params.node_name} | ${req.params.job.substring(0, 10)}.. | ${req.params.type} | ${req.params.number} | ${req.params.step} | ${req.params.status}`);

        db.all(`INSERT INTO jenkins_measurements (node_name, job, type, number, step, status) VALUES ('${req.params.node_name}', '${req.params.job}', '${req.params.type}', ${req.params.number}, '${req.params.step}', '${req.params.status}');`);
        
        return res.send("0");
    } catch (err) {
        console.log(err);
        return res.status(500).send("1");
    }
});

console.log(`App listening at port: ${port}...`)
app.listen(port);
