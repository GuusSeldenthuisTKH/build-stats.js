-- Total build-minutes for each day.
SELECT SUM(`duration`) as 'total_mins', `Day` 
FROM(
	SELECT
	Cast ((JulianDay(MAX(timestamp)) - JulianDay(MIN(timestamp))) * 24 * 60 As Integer) as 'duration',
	substr(timestamp,0,11) as 'day'
	FROM jenkins_measurements GROUP BY job, number)
GROUP BY Day;

-- For each day its average build-time, amount of builds and total build minutes.
SELECT Cast (AVG(duration) As Integer) as 'avg_mins', COUNT(*) as 'builds', SUM(`duration`) as 'total_mins', `Day` \
FROM( \
SELECT \
Cast ((JulianDay(MAX(timestamp)) - JulianDay(MIN(timestamp))) * 24 * 60 As Integer) as 'duration', \
substr(timestamp,0,11) as 'day' \
FROM jenkins_measurements GROUP BY job, number) \
GROUP BY Day;

-- Overview.
SELECT MIN(timestamp) as start, MAX(timestamp) as end, * FROM jenkins_measurements GROUP BY job, number ORDER BY MAX(timestamp);

SELECT job, COUNT(DISTINCT number) as 'builds' FROM jenkins_measurements GROUP BY job ORDER BY builds DESC;

SELECT Cast (AVG(duration) As Integer) as 'avg_mins', COUNT(*) as 'builds', SUM(`duration`) as 'total_mins', `Day`
FROM(
SELECT
Cast ((JulianDay(MAX(timestamp)) - JulianDay(MIN(timestamp))) * 24 * 60 As Integer) as 'duration',
substr(timestamp,0,11) as 'day'
FROM jenkins_measurements GROUP BY job, number)
GROUP BY Day;