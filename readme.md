# build-stats.js
## Settings

Default port: `1337`

## Usage

install required package(s) `npm i`

Start server: `node src/build-stats.js`

endpoint: `127.0.0.1:1337/node name/job name/build type/build number/step/status`

ps: the server listens to `0.0.0.0:1337` by default
